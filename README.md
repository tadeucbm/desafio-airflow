# Desafio Airflow 

### 1. Motivação do Projeto

Este é um repositório que contém os arquivos de resposta para o 
desafio de airflow do programada Lighthouse da Indicium. O objetivo foi 
desenvolver uma DAG capaz de extrair os dados do banco de dados 
sqlite da Northwind, salvar um csv dos resultados e fazer a contagem da coluna
'Quantity'. 

### 2. Instruções

1. Clone o repositório localmente:   
  
```
git clone git@bitbucket.org:tadeucbm/desafio-airflow.git
cd desafio-airflow
```

2. Crie um ambiente virtual;  

```
virtualenv venv -p python3
source venv/bin/activate
```

3. Instale as dependências do requirements.txt:   
  
```
pip install -r requirements.txt
```

4. Rode o arquivo install.sh   
  
```
bash install.sh
```

5. Entre com o login e senha acessando o caminho [http://0.0.0.0:8080/](http://0.0.0.0:8080/) e utilize a DAG.